##**Bitcoin Ticker**

The application was developed as part of job interview for oXyShop company.

###Build
Build project with command:

`mvn clean package` or without tests with `mvn clean package -DskipTests`

###Run
Run project with command:

``mvn spring-boot:run``

###Clone
1. Open terminal
2. Enter `git clone  https://bitbucket.org/ondoestokm/oxyshop-bitcoin-ticker.git`
3. Switch to repository's directory `cd oxyshop-bitcoin-ticker/`