package oxyshop.ticker.web;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import oxyshop.ticker.dto.BitcoinTickDto;
import oxyshop.ticker.service.BitcoinTickService;

import java.util.List;

/**
 * Exposed REST API for service layer
 */
@Slf4j
@RestController
@RequestMapping(path = "/api/tick")
public class BitcoinTickController {

    @Autowired
    private BitcoinTickService service;

    @GetMapping()
    public List<BitcoinTickDto> findPrevious(@RequestParam(value = "lastId", required = false, defaultValue = "0") long lastId) {
        log.info("Called findPrevious with {}", lastId);

        List<BitcoinTickDto> result = service.findPreviousInWeek(lastId);

        log.debug("Result from findPrevious {}", result);
        return result;
    }
}
