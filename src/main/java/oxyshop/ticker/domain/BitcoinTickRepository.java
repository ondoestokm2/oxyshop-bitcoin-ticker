package oxyshop.ticker.domain;

import org.springframework.data.jpa.repository.JpaRepository;

import java.time.LocalDateTime;
import java.util.List;

/**
 * Repository for querying domain objects {@link BitcoinTickEntity} extended by {@link JpaRepository }
 */
public interface BitcoinTickRepository extends JpaRepository<BitcoinTickEntity, Long> {

    /**
     * @param previousId id of previous object
     * @param dateTime   lower boundary of queried objects
     * @return list of {@link BitcoinTickEntity}
     */
    List<BitcoinTickEntity> findByIdGreaterThanAndTimestampAfter(Long previousId, LocalDateTime dateTime);
}
