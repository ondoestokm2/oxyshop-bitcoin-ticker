package oxyshop.ticker.domain;

import lombok.Data;

import javax.persistence.*;
import java.math.BigDecimal;
import java.time.LocalDateTime;

/**
 * Domain object of Bitcoin Tick
 */
@Data
@Entity
@Table(name = "oxy_bitcoin_tick")
public class BitcoinTickEntity {

    @Id
    @Column(name = "oxy_id")
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(name = "oxy_last")
    private BigDecimal last;

    @Column(name = "oxy_high")
    private BigDecimal high;

    @Column(name = "oxy_low")
    private BigDecimal low;

    @Column(name = "oxy_amount")
    private BigDecimal amount;

    @Column(name = "oxy_bid")
    private BigDecimal bid;

    @Column(name = "oxy_ask")
    private BigDecimal ask;

    @Column(name = "oxy_open")
    private BigDecimal open;

    @Column(name = "oxy_change")
    private BigDecimal change;

    @Column(name = "oxy_timestamp")
    private LocalDateTime timestamp;
}
