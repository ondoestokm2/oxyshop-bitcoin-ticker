package oxyshop.ticker.scheduler;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import oxyshop.ticker.service.BitcoinTickService;

/**
 * Component for automated tasks associated to Bitcoin Ticks
 */
@Slf4j
@Component
public class BitcoinTickScheduler {

    @Autowired
    private BitcoinTickService service;

    /**
     * Scheduler for fetch Bitcoin tick from remote api every 5 minutes and store it into database
     */
    @Scheduled(cron = "${ticker.cron}")
    public void fetchData() {
        log.info("Activated scheduled task for fetching remote data");
        service.fetchRemoteData();
    }
}
