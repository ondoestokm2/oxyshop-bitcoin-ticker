package oxyshop.ticker.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.io.Serializable;

/**
 * Domain transfer object wrapper for {@link BitcoinTickDto} received from remote
 */
@Data
public class BitcoinTickDetailDto implements Serializable {

    @JsonProperty("error")
    private boolean error;

    @JsonProperty("errorMessage")
    private String errorMessage;

    @JsonProperty("data")
    private BitcoinTickDto data;
}
