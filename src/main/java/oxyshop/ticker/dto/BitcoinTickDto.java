package oxyshop.ticker.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import lombok.Data;
import oxyshop.ticker.util.TimestampDeserializer;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDateTime;

/**
 * Domain transfer object for Bitcoin Tick
 */
@Data
public class BitcoinTickDto implements Serializable {

    @JsonProperty("id")
    private Long id;

    @JsonProperty("last")
    private BigDecimal last;

    @JsonProperty("high")
    private BigDecimal high;

    @JsonProperty("low")
    private BigDecimal low;

    @JsonProperty("amount")
    private BigDecimal amount;

    @JsonProperty("bid")
    private BigDecimal bid;

    @JsonProperty("ask")
    private BigDecimal ask;

    @JsonProperty("open")
    private BigDecimal open;

    @JsonProperty("change")
    private BigDecimal change;

    @JsonProperty("timestamp")
    @JsonDeserialize(using = TimestampDeserializer.class)
    private LocalDateTime timestamp;
}
