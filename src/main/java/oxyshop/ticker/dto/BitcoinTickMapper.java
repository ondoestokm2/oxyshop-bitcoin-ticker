package oxyshop.ticker.dto;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;
import oxyshop.ticker.domain.BitcoinTickEntity;

/**
 * Dto mapper for Bitcoin Tick objects
 */
@Mapper(componentModel = "spring")
public interface BitcoinTickMapper {

    /**
     * Mapper for mapping of {@link BitcoinTickDetailDto} to {@link BitcoinTickEntity}
     *
     * @param detailDto source object
     * @return mapped object
     */
    @Mappings({
            @Mapping(source = "data.last", target = "last"),
            @Mapping(source = "data.high", target = "high"),
            @Mapping(source = "data.low", target = "low"),
            @Mapping(source = "data.amount", target = "amount"),
            @Mapping(source = "data.bid", target = "bid"),
            @Mapping(source = "data.ask", target = "ask"),
            @Mapping(source = "data.open", target = "open"),
            @Mapping(source = "data.change", target = "change"),
            @Mapping(source = "data.timestamp", target = "timestamp"),
            @Mapping(target = "id", ignore = true)
    })
    BitcoinTickEntity detailDtoToEntity(BitcoinTickDetailDto detailDto);

    /**
     * Mapper for mapping {@link BitcoinTickEntity} to {@link BitcoinTickDto}
     *
     * @param detailDto source object
     * @return mapped object
     */
    BitcoinTickDto entityToDto(BitcoinTickEntity detailDto);
}
