package oxyshop.ticker.service;

import oxyshop.ticker.dto.BitcoinTickDto;

import java.util.List;

/**
 * Service for manipulation with  Bitcoin Ticks on business layer
 */
public interface BitcoinTickService {

    /**
     * Read tick objects in previous week by last received id
     *
     * @param lastId id of last received tick object
     * @return list of {@link BitcoinTickDto}
     */
    List<BitcoinTickDto> findPreviousInWeek(long lastId);

    /**
     * Fetch data from remote api
     */
    void fetchRemoteData();
}
