package oxyshop.ticker.service;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.client.RestTemplate;
import oxyshop.ticker.domain.BitcoinTickEntity;
import oxyshop.ticker.domain.BitcoinTickRepository;
import oxyshop.ticker.dto.BitcoinTickDetailDto;
import oxyshop.ticker.dto.BitcoinTickDto;
import oxyshop.ticker.dto.BitcoinTickMapper;

import java.net.URI;
import java.time.LocalDateTime;
import java.util.List;
import java.util.stream.Collectors;

@Slf4j
@Service
public class BitcoinTickServiceImpl implements BitcoinTickService {

    @Value("${ticker.url}")
    private URI apiUrl;

    @Value("${ticker.days}")
    private int days;

    @Autowired
    private RestTemplate restTemplate;

    @Autowired
    private BitcoinTickRepository repository;

    @Autowired
    private BitcoinTickMapper mapper;

    @Transactional(readOnly = true)
    @Override
    public List<BitcoinTickDto> findPreviousInWeek(long previousId) {
        log.info("Called findPreviousInWeek with {}", previousId);

        LocalDateTime fromDate = LocalDateTime.now().minusDays(days);
        List<BitcoinTickDto> result = repository.findByIdGreaterThanAndTimestampAfter(previousId, fromDate)
                .stream()
                .map(mapper::entityToDto)
                .collect(Collectors.toList());

        log.debug("Result from findPreviousInWeek {}", result);
        return result;
    }

    @Transactional
    @Override
    public void fetchRemoteData() {
        BitcoinTickDetailDto bitcoinTickDetailDto = restTemplate.getForObject(apiUrl, BitcoinTickDetailDto.class);
        log.info("Fetched data from remote {}", bitcoinTickDetailDto);

        BitcoinTickEntity entity = mapper.detailDtoToEntity(bitcoinTickDetailDto);
        BitcoinTickEntity savedEntity = repository.saveAndFlush(entity);
        log.debug("Saved fetched data {}", savedEntity);
    }
}
