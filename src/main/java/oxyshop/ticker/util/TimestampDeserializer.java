package oxyshop.ticker.util;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import lombok.extern.slf4j.Slf4j;

import java.io.IOException;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;

@Slf4j
/**
 * Deserializer for Unix timestamp
 */
public class TimestampDeserializer extends JsonDeserializer<LocalDateTime> {

    @Override
    public LocalDateTime deserialize(JsonParser jsonParser, DeserializationContext deserializationContext) {
        try {
            String timestamp = jsonParser.getText();
            return Instant.ofEpochSecond(Long.valueOf(timestamp))
                    .atZone(ZoneId.systemDefault())
                    .toLocalDateTime();

        } catch (IOException | NumberFormatException ex) {
            log.error(ex.getMessage());
        }
        return null;
    }
}
