$(function () {
    // variables init
    window.previousId = 0;
    window.apiUrl = 'http://localhost:8080/api/tick';
    window.days = 7;
    window.pollTime = 10000;

    let context = document.getElementById("btcChart").getContext('2d');
    context.canvas.width = 1000;
    context.canvas.height = 300;

    // chart init
    window.btcChart = new Chart(context, {
        type: 'line',
        data: {
            datasets: [{
                lineTension: 0.1,
                fill: true,
                borderColor: 'orange',
                fillColor: 'orange',
                data: []
            }]
        },
        options: {
            legend: {
                display: false
            },
            scales: {
                xAxes: [{
                    type: 'time',
                    time: {
                        unit: 'day',
                        displayFormats: {
                            day: 'DD.MM.YYYY'
                        }
                    },
                }],
                yAxes: [{
                    ticks: {
                        source: 'data',
                        beginAtZero: true
                    },
                }]
            }
        }
    });

    // start polling
    poll();
});

function poll() {
    $.ajax({
        url: window.apiUrl,
        type: "GET",
        data: {
            lastId: window.lastId
        },
        dataType: "json",
        success: onSuccess,
        complete: setTimeout(poll, window.pollTime)
    });
}

function onSuccess(response) {
    let oldTicks = null;
    let newTicks = null;
    let lastTick = null;
    let defaultValue = '\u2014';
    let greenTextClass = 'text-success';
    let redTextClass = 'text-danger';
    let deprecatedCount = 0;

    if (_.isArray(response) && !_.isEmpty(response)) {

        oldTicks = window.btcChart.config.data.datasets[0].data;
        newTicks = _(response).orderBy('id', 'asc')
            .map(item => (
                {
                    'id': item.id,
                    'last': item.last,
                    'high': item.high,
                    'low': item.low,
                    'change': item.change,
                    'x': moment(item.timestamp),
                    'y': item.last
                }
            )).value();
        lastTick = _.maxBy(newTicks, 'x');

        // find out count of deprecated ticks in chart
        _.forEach(oldTicks, item => {
            if (lastTick.x.diff(item.x, 'days') >= window.days) {
                deprecatedCount++;
            } else {
                return;
            }
        });

        // remove deprecated in chart
        window.btcChart.config.data.datasets[0].data.splice(0, deprecatedCount);

        // push new data
        _.forEach(newTicks, item => window.btcChart.config.data.datasets[0].data.push(item));

        // update chart
        window.btcChart.update();

        // update lastId for next api call
        window.lastId = lastTick.id;
    }

    // update page info
    $("#lastValue").text(() => lastTick && lastTick.last ? lastTick.last : defaultValue);
    $("#highValue").text(() => lastTick && lastTick.high ? lastTick.high : defaultValue);
    $("#lowValue").text(() => lastTick && lastTick.low ? lastTick.low : defaultValue);
    $("#changeValue").text(() => lastTick && lastTick.change ? lastTick.change : defaultValue);
    $("#changeFormattedValue").removeClass();
    let cssClass = lastTick && lastTick.change >= 0 ? greenTextClass : redTextClass;
    $("#changeFormattedValue").addClass(cssClass);
}
