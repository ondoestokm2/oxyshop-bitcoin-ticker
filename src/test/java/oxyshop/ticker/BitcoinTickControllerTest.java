package oxyshop.ticker;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import oxyshop.ticker.dto.BitcoinTickDto;
import oxyshop.ticker.service.BitcoinTickService;
import oxyshop.ticker.web.BitcoinTickController;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.Collections;

import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@WebMvcTest(BitcoinTickController.class)
public class BitcoinTickControllerTest {

    @Autowired
    private MockMvc mvc;

    @MockBean
    private BitcoinTickService service;

    @Test
    public void test() throws Exception {
        int last = 1000;
        int high = 2000;
        int low = 3000;
        int amount = 4000;
        int bid = 5000;
        int ask = 6000;
        int open = 7000;
        int change = 8000;
        LocalDateTime timestamp = LocalDateTime.of(2018, 1, 1, 11, 11, 11);

        BitcoinTickDto tickDto = new BitcoinTickDto();
        tickDto.setId(1L);
        tickDto.setLast(BigDecimal.valueOf(last));
        tickDto.setHigh(BigDecimal.valueOf(high));
        tickDto.setLow(BigDecimal.valueOf(low));
        tickDto.setAmount(BigDecimal.valueOf(amount));
        tickDto.setBid(BigDecimal.valueOf(bid));
        tickDto.setAsk(BigDecimal.valueOf(ask));
        tickDto.setOpen(BigDecimal.valueOf(open));
        tickDto.setChange(BigDecimal.valueOf(change));
        tickDto.setTimestamp(timestamp);

        when(service.findPreviousInWeek(0)).thenReturn(Collections.singletonList(tickDto));

        mvc.perform(get("/api/tick")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", hasSize(1)))
                .andExpect(jsonPath("$[0].id", is(1)))
                .andExpect(jsonPath("$[0].last", is(last)))
                .andExpect(jsonPath("$[0].high", is(high)))
                .andExpect(jsonPath("$[0].low", is(3000)))
                .andExpect(jsonPath("$[0].amount", is(amount)))
                .andExpect(jsonPath("$[0].bid", is(bid)))
                .andExpect(jsonPath("$[0].ask", is(ask)))
                .andExpect(jsonPath("$[0].open", is(open)))
                .andExpect(jsonPath("$[0].change", is(change)))
                .andExpect(jsonPath("$[0].timestamp", is("2018-01-01T11:11:11")));

        verify(service).findPreviousInWeek(0);
    }
}
