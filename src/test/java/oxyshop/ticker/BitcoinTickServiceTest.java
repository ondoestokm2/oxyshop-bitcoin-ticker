package oxyshop.ticker;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.boot.test.mock.mockito.SpyBean;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.web.client.RestTemplate;
import oxyshop.ticker.domain.BitcoinTickEntity;
import oxyshop.ticker.domain.BitcoinTickRepository;
import oxyshop.ticker.dto.BitcoinTickDetailDto;
import oxyshop.ticker.dto.BitcoinTickDto;
import oxyshop.ticker.dto.BitcoinTickMapper;
import oxyshop.ticker.service.BitcoinTickService;

import java.math.BigDecimal;
import java.net.URI;
import java.time.LocalDateTime;
import java.util.Collections;
import java.util.List;

import static org.hamcrest.Matchers.hasSize;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.*;

@SpringBootTest
@RunWith(SpringRunner.class)
public class BitcoinTickServiceTest {

    @MockBean
    private BitcoinTickRepository repository;

    @SpyBean
    private BitcoinTickMapper mapper;

    @MockBean
    private RestTemplate restTemplate;

    @SpyBean
    private BitcoinTickService service;

    @Test
    public void findPreviousInWeekTest() {
        long lastId = 2L;
        long id = 3L;
        int last = 1000;
        int high = 2000;
        int low = 3000;
        int amount = 4000;
        int bid = 5000;
        int ask = 6000;
        int open = 7000;
        int change = 8000;
        LocalDateTime timestamp = LocalDateTime.of(2018, 1, 1, 11, 11, 11);

        BitcoinTickEntity tickEntity = new BitcoinTickEntity();
        tickEntity.setId(id);
        tickEntity.setLast(BigDecimal.valueOf(last));
        tickEntity.setHigh(BigDecimal.valueOf(high));
        tickEntity.setLow(BigDecimal.valueOf(low));
        tickEntity.setAmount(BigDecimal.valueOf(amount));
        tickEntity.setBid(BigDecimal.valueOf(bid));
        tickEntity.setAsk(BigDecimal.valueOf(ask));
        tickEntity.setOpen(BigDecimal.valueOf(open));
        tickEntity.setChange(BigDecimal.valueOf(change));
        tickEntity.setTimestamp(timestamp);

        when(repository.findByIdGreaterThanAndTimestampAfter(eq(lastId), any(LocalDateTime.class)))
                .thenReturn(Collections.singletonList(tickEntity));
        when(mapper.entityToDto(any())).thenCallRealMethod();

        List<BitcoinTickDto> serviceResult = service.findPreviousInWeek(lastId);

        verify(repository).findByIdGreaterThanAndTimestampAfter(eq(lastId), any());
        assertThat(serviceResult, hasSize(1));
        BitcoinTickDto tickDto = serviceResult.get(0);
        assertEquals(id, tickDto.getId().longValue());
        assertEquals(last, tickDto.getLast().intValue());
        assertEquals(high, tickDto.getHigh().intValue());
        assertEquals(low, tickDto.getLow().intValue());
        assertEquals(amount, tickDto.getAmount().intValue());
        assertEquals(bid, tickDto.getBid().intValue());
        assertEquals(ask, tickDto.getAsk().intValue());
        assertEquals(open, tickDto.getOpen().intValue());
        assertEquals(change, tickDto.getChange().intValue());
    }


    @Test
    public void fetchRemoteDataTest() {
        BitcoinTickEntity entityMock = mock(BitcoinTickEntity.class);
        int last = 1000;
        int high = 2000;
        int low = 3000;
        int amount = 4000;
        int bid = 5000;
        int ask = 6000;
        int open = 7000;
        int change = 8000;
        LocalDateTime timestamp = LocalDateTime.of(2018, 1, 1, 11, 11, 11);
        boolean isError = false;
        String errorMessage = null;

        BitcoinTickDto tickDto = new BitcoinTickDto();
        tickDto.setId(1L);
        tickDto.setLast(BigDecimal.valueOf(last));
        tickDto.setHigh(BigDecimal.valueOf(high));
        tickDto.setLow(BigDecimal.valueOf(low));
        tickDto.setAmount(BigDecimal.valueOf(amount));
        tickDto.setBid(BigDecimal.valueOf(bid));
        tickDto.setAsk(BigDecimal.valueOf(ask));
        tickDto.setOpen(BigDecimal.valueOf(open));
        tickDto.setChange(BigDecimal.valueOf(change));
        tickDto.setTimestamp(timestamp);

        BitcoinTickDetailDto tickDetailDto = new BitcoinTickDetailDto();
        tickDetailDto.setError(isError);
        tickDetailDto.setErrorMessage(errorMessage);

        when(restTemplate.getForObject(any(URI.class), eq(BitcoinTickDetailDto.class))).thenReturn(tickDetailDto);
        when(mapper.detailDtoToEntity(eq(tickDetailDto))).thenReturn(entityMock);

        service.fetchRemoteData();

        verify(mapper).detailDtoToEntity(tickDetailDto);
        verify(repository).saveAndFlush(entityMock);
    }
}
